import java.io.*;
import java.util.*;
import java.lang.Math;

public class Exam {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();

        int[][] matrix = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                findMax(matrix);
            }
        }
    }

    static void findMax(int M[][]) {
        int i, j;
        int R = M.length;
        int C = M[0].length;
        int S[][] = new int[R][C];

        int max_of_s, max_i, max_j;

        for (i = 0; i < R; i++)
            S[i][0] = M[i][0];

        for (j = 0; j < C; j++)
            S[0][j] = M[0][j];

        for (i = 1; i < R; i++) {
            for (j = 1; j < C; j++) {
                if (M[i][j] == 1)
                    S[i][j] = Math.min(S[i][j - 1], Math.min(S[i - 1][j], S[i - 1][j - 1])) + 1;
                else
                    S[i][j] = 0;
            }
        }

        max_of_s = S[0][0];
        max_i = 0;
        max_j = 0;
        for (i = 0; i < R; i++) {
            for (j = 0; j < C; j++) {
                if (max_of_s < S[i][j]) {
                    max_of_s = S[i][j];
                    max_i = i;
                    max_j = j;
                }
            }
        }

        System.out.println(max_of_s);
    }

}
